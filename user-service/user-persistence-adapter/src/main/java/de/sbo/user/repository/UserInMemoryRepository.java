package de.sbo.user.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import de.sbo.user.AddressVO;
import de.sbo.user.PersonalDataVO;
import de.sbo.user.UserRepository;
import de.sbo.user.UserVO;

@Component
public class UserInMemoryRepository implements UserRepository{

	@Override
	public List<UserVO> findAll() {
		return getAllUsers();
	}
	
	
	private List<UserVO> getAllUsers(){
		List<UserVO> users = new ArrayList<>();
		users.add(new UserVO("1", new PersonalDataVO("Hans", "Mueller"), new AddressVO("Testweg", "13", "Testdorf", "Deutschaland")));
		return users;
	}
}
