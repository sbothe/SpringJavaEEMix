package de.sbo.user.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.sbo.user.UserService;
import de.sbo.user.UserVO;


@RestController
@RequestMapping("/api/v1")
public class UserController {
	
	 Logger logger = LoggerFactory.getLogger(UserController.class);
	 
	 @Autowired
	 private UserService userSevice;
		
	  @GetMapping("/user")
	  ResponseEntity<List<UserVO>> getUser(@RequestParam(value = "userId") String userId) {
		if(userId == null || userId.isBlank()){
			logger.error("ERROR: User Id should not be empty.");
			return ResponseEntity.badRequest().build();
		}
	    return ResponseEntity.ok().body(userSevice.findAllUsers());
	  }
}
