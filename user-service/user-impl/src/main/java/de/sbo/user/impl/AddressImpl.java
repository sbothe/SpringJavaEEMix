package de.sbo.user.impl;

import de.sbo.user.Address;
import de.sbo.user.AddressVO;
import de.sbo.user.User;
import de.sbo.user.UserVO;
import lombok.Getter;

@Getter
public class AddressImpl implements Address{
	
	private String street;
	private String houseNumber;
	private String city;
	private String country;
	
	public static Address fromVO(AddressVO vo){
		return new AddressImpl(vo);
	}
	
	@Override
	public AddressVO toVO(){
		return new AddressVO(this.street,this.houseNumber,this.city,this.country);
	}
	
	private AddressImpl(AddressVO vo)  {
		this.street = vo.getStreet();
		this.houseNumber = vo.getHouseNumber();
		this.city = vo.getCity();
		this.country = vo.getCountry();
	}
}
