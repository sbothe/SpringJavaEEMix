package de.sbo.user.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.sbo.user.UserRepository;
import de.sbo.user.UserService;
import de.sbo.user.UserVO;

@Component
public class UserServiceImpl implements UserService{

	
	@Autowired
	private UserRepository userRepository;
	
	
	@Override
	public List<UserVO> findAllUsers(){
		return this.userRepository.findAll();
	}
}
