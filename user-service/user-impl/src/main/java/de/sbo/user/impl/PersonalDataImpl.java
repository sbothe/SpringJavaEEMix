package de.sbo.user.impl;

import de.sbo.user.Address;
import de.sbo.user.AddressVO;
import de.sbo.user.PersonalData;
import de.sbo.user.PersonalDataVO;
import lombok.Getter;

@Getter
public class PersonalDataImpl implements PersonalData{

	private String firstname;
	private String lastname;
	
	public static PersonalData fromVO(PersonalDataVO vo){
		return new PersonalDataImpl(vo);
	}
	
	@Override
	public PersonalDataVO toVO(){
		return new PersonalDataVO(this.firstname,this.lastname);
	}
	
	private PersonalDataImpl(PersonalDataVO vo)  {
		this.firstname = vo.getFirstname();
		this.lastname = vo.getLastname();
	}
}
