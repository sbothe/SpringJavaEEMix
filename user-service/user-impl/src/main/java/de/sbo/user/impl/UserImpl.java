package de.sbo.user.impl;

import de.sbo.user.Address;
import de.sbo.user.PersonalData;
import de.sbo.user.User;
import de.sbo.user.UserVO;
import lombok.Getter;

@Getter
public class UserImpl implements User {
	
	private String userId;
	private Address address;
	private PersonalData personalData;
	
	
	public static User fromVO(UserVO vo){
		return new UserImpl(vo);
	}
	
	public UserVO toVO(){
		return new UserVO(this.userId, this.personalData.toVO(), this.address.toVO());
	}
	
	
	private UserImpl(UserVO vo)  {
		this.userId = vo.getUserId();
		this.address = AddressImpl.fromVO(vo.getAddress());
		this.personalData = PersonalDataImpl.fromVO(vo.getPersonalData());
	}   
    
}
