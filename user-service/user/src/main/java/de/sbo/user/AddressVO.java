package de.sbo.user;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class AddressVO {
	String street;
	String houseNumber;
	String city;
	String country;
}
