package de.sbo.user;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;

@Value
@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PersonalDataVO {
	String firstname;
	String lastname;
}
