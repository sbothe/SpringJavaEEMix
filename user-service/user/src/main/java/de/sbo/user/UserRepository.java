package de.sbo.user;

import java.util.List;

public interface UserRepository {

	public List<UserVO> findAll();
	
}
