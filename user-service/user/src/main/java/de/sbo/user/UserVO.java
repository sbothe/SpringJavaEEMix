package de.sbo.user;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class UserVO {
	String userId;
	PersonalDataVO personalData;
	AddressVO address;
}
