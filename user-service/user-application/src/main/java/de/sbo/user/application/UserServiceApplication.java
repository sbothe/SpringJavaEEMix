package de.sbo.user.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import de.sbo.user.UserService;
import de.sbo.user.api.AliveController;
import de.sbo.user.impl.UserServiceImpl;
import de.sbo.user.repository.UserInMemoryRepository;

@SpringBootApplication
@ComponentScan(basePackageClasses = {AliveController.class, UserService.class, UserServiceImpl.class,UserInMemoryRepository.class})
public class UserServiceApplication {

	public static void main(String[] args) {

	//test223
		SpringApplication.run(UserServiceApplication.class, args);
	}

}
