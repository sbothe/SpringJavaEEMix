package de.sbo.bill;


import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import lombok.Getter;
import lombok.Setter;

@Named
@RequestScoped
public class BillingBean {
	
	@Getter
	@Setter
	private String name = "test";

	@Inject
	private IBillingController billingController;
	
	public List<BillVO> getBills() {
		return billingController.getAllBills();
	}


}
