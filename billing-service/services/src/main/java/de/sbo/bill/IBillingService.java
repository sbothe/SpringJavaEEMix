package de.sbo.bill;

import java.util.List;

public interface IBillingService {
	List<BillVO> getAllBills();
}
