package de.sbo.bill;

import java.util.List;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

@Model
public class BillingService implements IBillingService {

	@Inject
	private BillingRepsitory billingRepository;
	
	public List<BillVO> getAllBills(){
		return billingRepository.findAll();
	}
}
