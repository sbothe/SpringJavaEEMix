package de.sbo.bill;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class BillVO {
	String billingNumber;
	Float sum;
	String orderId;
}
