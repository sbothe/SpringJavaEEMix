package de.sbo.bill;

import java.util.List;

public interface IBillingRepository {
	List<BillVO> findAll();
}
