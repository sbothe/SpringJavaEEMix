package de.sbo.bill;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Model;
import javax.inject.Named;

@Model
public class BillingRepsitory implements IBillingRepository {
	
	public List<BillVO> findAll(){
		return getStaticBills();	
	}
	
	private List<BillVO> getStaticBills(){
		List<BillVO> bills = new ArrayList<>();
		bills.add(new BillVO("1", Float.parseFloat("22.22"), "1"));
		return bills;
	}
}
