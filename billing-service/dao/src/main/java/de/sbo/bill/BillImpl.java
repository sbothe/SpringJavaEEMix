package de.sbo.bill;

import lombok.Getter;

@Getter
public class BillImpl implements Bill {

	private String billingNumber;
	private Float sum;
	private String orderId;
	
	public static Bill fromVO(BillVO vo){
		return new BillImpl(vo);
	}
	
	public BillVO toVO(){
		return new BillVO(this.billingNumber, this.sum, this.orderId);
	}
	
	private BillImpl(BillVO vo)  {
		this.billingNumber = vo.getBillingNumber();
		this.sum = vo.getSum();
		this.orderId = vo.getOrderId();
	}  
}
