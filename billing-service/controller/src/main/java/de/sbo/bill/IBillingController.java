package de.sbo.bill;

import java.util.List;

public interface IBillingController {
	List<BillVO> getAllBills();
}
