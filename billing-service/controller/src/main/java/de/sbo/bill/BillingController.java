package de.sbo.bill;

import java.util.List;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

@Model
public class BillingController implements IBillingController{

	@Inject
	private BillingService billingService;
	
	public List<BillVO> getAllBills(){
		return billingService.getAllBills();
	}
}
