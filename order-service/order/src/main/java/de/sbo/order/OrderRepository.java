package de.sbo.order;

import java.util.List;

public interface OrderRepository {

	public List<OrderVO> findAll();
	
}
