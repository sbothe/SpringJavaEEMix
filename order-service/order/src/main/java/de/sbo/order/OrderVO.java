package de.sbo.order;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;

@Value
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class OrderVO {
	String orderId;
	Float price;
	String orderedBy;
}
