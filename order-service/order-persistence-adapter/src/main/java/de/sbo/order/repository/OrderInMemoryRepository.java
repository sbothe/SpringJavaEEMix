package de.sbo.order.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import de.sbo.order.OrderRepository;
import de.sbo.order.OrderVO;

@Component
public class OrderInMemoryRepository implements OrderRepository{

	@Override
	public List<OrderVO> findAll() {
		return getAllOrders();
	}
	
	private List<OrderVO> getAllOrders(){
		List<OrderVO> orders = new ArrayList<>();
		orders.add(new OrderVO("1", Float.parseFloat("22.22") , "1"));
		return orders;
	}
}
