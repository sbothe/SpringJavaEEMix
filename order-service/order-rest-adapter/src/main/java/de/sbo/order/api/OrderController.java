package de.sbo.order.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.sbo.order.OrderService;
import de.sbo.order.OrderVO;


@RestController
@RequestMapping("/api/v1")
public class OrderController {
	
      Logger logger = LoggerFactory.getLogger(OrderController.class);
      
      @Autowired
      private OrderService orderService;
	
	  @GetMapping("/order")
	  ResponseEntity<List<OrderVO>> getAllOrdersForUser(@RequestParam(value = "userId") String userId) {
		if(userId == null || userId.isBlank()){
			logger.error("ERROR: User Id should not be empty.");
			return ResponseEntity.badRequest().build();
		}
	    return ResponseEntity.ok().body(orderService.findAll());
	  }
}
