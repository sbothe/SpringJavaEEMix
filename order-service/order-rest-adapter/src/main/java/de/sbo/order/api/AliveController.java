package de.sbo.order.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class AliveController {
	
	  Logger logger = LoggerFactory.getLogger(OrderController.class);
	
	  @GetMapping("/alive")
	  ResponseEntity<String> isAlive() {	  
		logger.info("Alive status check performed.");  
	    return ResponseEntity.ok().body("order service alive!!");
	  }
}
