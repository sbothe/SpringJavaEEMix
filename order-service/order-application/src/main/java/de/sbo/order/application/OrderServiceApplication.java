package de.sbo.order.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import de.sbo.order.OrderService;
import de.sbo.order.api.AliveController;
import de.sbo.order.impl.OrderServiceImpl;
import de.sbo.order.repository.OrderInMemoryRepository;

@SpringBootApplication
@ComponentScan(basePackageClasses = {AliveController.class, OrderService.class, OrderServiceImpl.class,OrderInMemoryRepository.class})
public class OrderServiceApplication {

	public static void main(String[] args) {

	//test223
		SpringApplication.run(OrderServiceApplication.class, args);
	}

}
