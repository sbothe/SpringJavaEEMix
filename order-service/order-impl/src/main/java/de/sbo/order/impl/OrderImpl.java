package de.sbo.order.impl;

import de.sbo.order.Order;
import de.sbo.order.OrderVO;
import lombok.Getter;

@Getter
public class OrderImpl implements Order {

	private String orderId;
	private Float price;
	private String orderedBy;
	

	public static Order fromVO(OrderVO vo){
		return new OrderImpl(vo);
	}
	
	public OrderVO toVO(){
		return new OrderVO(this.orderId, this.price, this.orderedBy);
	}
	
	private OrderImpl(OrderVO vo)  {
		this.orderId = vo.getOrderId();
		this.price = vo.getPrice();
		this.orderedBy = vo.getOrderedBy();
	}   
}
