package de.sbo.order.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.sbo.order.OrderRepository;
import de.sbo.order.OrderService;
import de.sbo.order.OrderVO;

@Component
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;
	
	@Override
	public List<OrderVO> findAll() {
		return orderRepository.findAll();
	}

}
