# SpringJavaEEMix
This repository contains a demo implementation of a proof of concept migration from a monolithic JavaEE application, which contains a n-tier architecture towards a more domain driven approach, containing spring boot.
## Scenario
The demo app is a monolithic JavaEE application which is responsible for users placing orders and getting bills for those orders. The analysis of bounded contexts identified three main contexts during the analysis. Those three contexts are: Billling, order-placement and user-management. This example should model an intermediate state of the migration, due to the assumption that a big bang migration is not possible, because the demo app is a running system in production which is under development and needs releases during the migration process.
## Assumptions
- A big bang migration is not possible.
- A greenfield scenario is not given.
- The project is stuck to the current infrastructure restrictions (JavaEE Application Server WAS Liberty).
- A Service split up does not make sense under those circumstances because a single EAR is deployed.
- An EAR can contain multiple WAR Archives.
- The communication between the contexts by using HTTP does not make sense until the sevices are running an sparate servers.
## Technical background
- The demo decided to use gradle but could also be build on maven as alternative.
- Spring boot is chosen to design all new bounded contexts
- The old contexts are using JavaEE CDI Beans
- The new designed contexts should stick towards hexagonal architecture
## Order-Service
- Hexagonal architecture
- based on Spring boot WAR Starter
- Gradle based
- Modules: Domain-Interfaces, Domain-Implementation, REST-Adapter, Persistence-Adapter
## Billing-Service
- N-Tier architecture
- JavaEE based
- No EJBs
- Modules: Web, Services, dao
## User-Service
- Hexagonal architecture
- based on Spring boot WAR Starter
- Gradle based
- Modules: Domain-Interfaces, Domain-Implementation, REST-Adapter, Persistence-Adapter
